---
id: writing-a-solid-app
title: Writing a Solid App
---

This tutorial assumes you are familiar with modern Javascript and its ecosystem (e.g. you know how
and when to use tools like npm, Webpack, etc.), know how to write regular Web Apps, and are
interested in building one that stores data to a [Solid](https://solidproject.org/) Pod.

It does not assume prior knowledge of Semantic Web technologies, Linked Data, RDF, or knowing what
those terms mean in the first place.

We will be building [a note-taking app](https://notepod.vincenttunru.com/) that can add notes to
someone's Pod, and can read existing ones. If you're so inclined, you can [play around with the
source code of the finished
app](https://codesandbox.io/s/github/Vinnl/notepod/tree/5-writing-data/?module=%2Fsrc%2FApp.tsx)
already.
